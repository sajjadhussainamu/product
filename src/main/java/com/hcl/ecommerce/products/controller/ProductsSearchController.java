package com.hcl.ecommerce.products.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ecommerce.products.entity.dto.ProductResponseDto;
import com.hcl.ecommerce.products.service.ProductsSearchService;

@RestController
@RequestMapping("search")
public class ProductsSearchController {

	@Autowired
	ProductsSearchService searchService;

	@GetMapping("/product_by_name/{name}")
	public ProductResponseDto getProductByName(@PathVariable("name") String name) {

		ProductResponseDto products = searchService.getProductsByName(name);
		return products;

	}

	@GetMapping("/product_by_id/{id}")
	public ProductResponseDto getProductById(@PathVariable long id) {

		ProductResponseDto productResponseDto = searchService.getProductById(id);
		return productResponseDto;

	}

	@GetMapping("/product_by_category_id/{categoryId}")
	public ProductResponseDto getProductByCategoryId(@PathVariable("categoryId") long categoryId) {

		ProductResponseDto productResponseDto = searchService.getProductByCategoryId(categoryId);

		return productResponseDto;

	}

}
