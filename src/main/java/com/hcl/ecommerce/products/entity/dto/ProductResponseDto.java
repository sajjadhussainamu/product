package com.hcl.ecommerce.products.entity.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductResponseDto {

	List<ProductDto> productDtos = new ArrayList<>();

}
