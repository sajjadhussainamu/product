package com.hcl.ecommerce.products.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.ecommerce.products.entity.Product;

@Repository
public interface ProductsSearchRepository   extends JpaRepository<Product, Long> {

	List<Product> findByProductNameContains(String categoryName);

	List<Product> findByCategoryId(long categoryId);

}
