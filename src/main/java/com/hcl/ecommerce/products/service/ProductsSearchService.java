package com.hcl.ecommerce.products.service;

import com.hcl.ecommerce.products.entity.dto.ProductResponseDto;

public interface ProductsSearchService {

	ProductResponseDto getProductsByName(String name);

	ProductResponseDto getProductById(long id);

	ProductResponseDto getProductByCategoryId(long categoryId);

}
