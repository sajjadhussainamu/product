package com.hcl.ecommerce.products.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.ecommerce.products.entity.Product;
import com.hcl.ecommerce.products.entity.dto.ProductDto;
import com.hcl.ecommerce.products.entity.dto.ProductResponseDto;
import com.hcl.ecommerce.products.repository.ProductsSearchRepository;

@Service
public class ProductsSearchServiceImpl implements ProductsSearchService {

	@Autowired
	ProductsSearchRepository searchRepository;

	@Override
	public ProductResponseDto getProductsByName(String categoryName) {
		ProductResponseDto dtProductResponseDto = new ProductResponseDto();

		List<Product> products = searchRepository.findByProductNameContains(categoryName);
		if (products.isEmpty()) {
			return dtProductResponseDto;
		}
		List<ProductDto> productDtos = new ArrayList<>();
		for (Product product : products) {
			ProductDto productDto = new ProductDto();
			BeanUtils.copyProperties(product, productDto);
			productDtos.add(productDto);
		}

		dtProductResponseDto.setProductDtos(productDtos);
		return dtProductResponseDto;
	}

	@Override
	public ProductResponseDto getProductById(long id) {
		Optional<Product> product = searchRepository.findById(id);
		ProductResponseDto dtProductResponseDto = new ProductResponseDto();
		if (!product.isPresent()) {

			return dtProductResponseDto;
		}
		ProductDto productDto = new ProductDto();
		BeanUtils.copyProperties(product.get(), productDto);
		List<ProductDto> productDtos = new ArrayList<>();
		productDtos.add(productDto);

		dtProductResponseDto.setProductDtos(productDtos);

		return dtProductResponseDto;

	}

	@Override
	public ProductResponseDto getProductByCategoryId(long categoryId) {
		List<ProductDto> productDtos = new ArrayList<>();
		List<Product> products = searchRepository.findByCategoryId(categoryId);

		for (Product product : products) {
			ProductDto productDto = new ProductDto();
			BeanUtils.copyProperties(product, productDto);
			productDtos.add(productDto);
		}
		ProductResponseDto dtProductResponseDto = new ProductResponseDto();
		dtProductResponseDto.setProductDtos(productDtos);
		return dtProductResponseDto;
	}
}
